.PHONY: build


build: compilemessages
	python setup.py sdist bdist_wheel

makemigrations:
	python manage.py makemigrations --no-header

migrate:
	python manage.py migrate

makemessages:
	python manage.py makemessages -l de
	python manage.py makemessages -l en

compilemessages:
	python manage.py compilemessages

runserver:
	python manage.py runserver
