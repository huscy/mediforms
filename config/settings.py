from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent


SECRET_KEY = 'django-insecure-s8(57@eah@j^o7ix3oeim^r*ccc-mug%2y^*cn4h62cadf#6-3'

DEBUG = True

ALLOWED_HOSTS = []


INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'mpi_cbs.mediforms.apps.HuscyApp',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'config.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}


DATE_INPUT_FORMATS = [
    '%d/%m/%Y',
    '%d.%m.%Y',
]


TIME_ZONE = 'UTC'

USE_I18N = True

USE_TZ = True


STATIC_URL = 'static/'

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'


LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/'

LANGUAGE_CODE = 'de-DE'

USE_L10N = True

LANGUAGES = (
    ('de-DE', 'German'),
    ('en-GB', 'English'),
)

LOCALE_PATHS = (
    BASE_DIR / 'mpi_cbs/mediforms/locale',
)

MEDIA_ROOT = BASE_DIR / 'media'


EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
MEDIFORMS_EMAIL_FROM = 'digitalconsent-admins@cbs.mpg.de'
MEDIFORMS_EMAIL_RECIPIENTS_LIST_TO = ['studienaerzte@cbs.mpg.de']
MEDIFORMS_EMAIL_RECIPIENTS_LIST_BCC = ['bunde@cbs.mpg.de']
MEDIFORMS_EMAIL_RECIPIENTS_LIST_REPLY_TO = ['studienaerzte@cbs.mpg.de']
MEDIFORMS_EMAIL_SUBJECT = 'TEST - mediforms Aufklärungsbogen'
