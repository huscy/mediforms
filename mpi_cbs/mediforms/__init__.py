# major.minor.patch.release.number
# release must be one of alpha, beta, rc or final
VERSION = (0, 3, 1, 'rc', 30)

__version__ = '.'.join(str(x) for x in VERSION)
